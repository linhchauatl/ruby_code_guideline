### This is a set of documents about Ruby coding guideline.

[1. Ruby naming conventions](https://bitbucket.org/linhchauatl/ruby_code_guideline/src/master/ruby_naming_conventions.md)

[2. Ruby coding guideline](https://bitbucket.org/linhchauatl/ruby_code_guideline/src/master/ruby_coding_guideline.md)

[3. Ruby implementation best practice](https://bitbucket.org/linhchauatl/ruby_code_guideline/src/master/ruby_implementation_best_practice.md)

There is a very detailed and comprehensive [**Ruby Style Guide**](https://github.com/bbatsov/ruby-style-guide).<br>
But it is too long for a development team to use it as a guideline for everyday job. It should be used as a source of reference and continuous learning.<br>
There are several heavy-handed points in that documents that I do not exactly agree. 